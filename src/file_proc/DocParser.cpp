#include "common.hpp"
#include "DocParser.hpp"
#include <fstream>
#include <sstream>
#include <future>
#include <vector>

#define MAX_THREAT 10

std::mutex GlobDictAcess;
std::mutex ThreatNumberAccess;
int threatNum = 0;

/*********************************************************************************
* DocParser Functions
*********************************************************************************/
std::map<std::string, int> DocParser::DocProcessing(std::string text, size_t docId){
    std::stringstream inStream(text);
    std::string word;
    std::map<std::string, int> wordsFreaq;

    //Control Number of threate
    ThreatNumberAccess.lock();
    threatNum++;
    ThreatNumberAccess.unlock();

    while(std::getline(inStream, word, ' ')){
        if(word.empty()) continue;
        if(wordsFreaq.find(word) != wordsFreaq.end()) 
            wordsFreaq[word]++;
        else
           wordsFreaq.insert(std::pair<std::string, int>(word, 1));

    }
    if(docId >= 0) UpdateGlobDict(wordsFreaq, docId);
    
    ThreatNumberAccess.lock();
    threatNum--;
    ThreatNumberAccess.unlock();

    return wordsFreaq;
}
/*********************************************************************************
* 
*********************************************************************************/
void DocParser::UpdateGlobDict(std::map<std::string, int> locDict, size_t docId)
{
    std::map<std::string, std::vector<Entry>>::iterator curWord;

    GlobDictAcess.lock();
    
    for(auto &word: locDict){
        Entry newNote(docId, word.second);
        if((curWord = freqDictionary.find(word.first)) != freqDictionary.end()){
            curWord->second.push_back(newNote);
            continue;
        }
        std::vector<Entry> newEntryVec{newNote};
        freqDictionary.insert(std::pair<std::string, std::vector<Entry>>(word.first, newEntryVec));
    }
    GlobDictAcess.unlock();
}
/*********************************************************************************
* InvertetIndex Functions
*********************************************************************************/
void InvertedIndex::UpdateDocumentBase(std::vector<std::string> inputDocs)
{
    size_t id = 0;
    std::vector<std::thread> indFiles;
    auto emptyDoc =[](std::vector<std::string> &docs){
        docs.push_back(std::string());
    };
    auto joinAllThreads =[&](){
       for(auto &file : indFiles) {
                if(file.joinable()) file.join();
            } 
    };

    for(auto &text: inputDocs){
        if(text.empty()) {
            emptyDoc(docs);
            continue;
        }
        
        ThreatNumberAccess.lock();
        while (MAX_THREAT <= threatNum)
        {
            ThreatNumberAccess.unlock();
            joinAllThreads();
            indFiles.clear();        
            ThreatNumberAccess.lock();
        }
        ThreatNumberAccess.unlock();
        indFiles.emplace_back(std::thread(&DocProcessing, this, std::ref(text), id));
        docs.push_back(text);
        id++;
    }
    joinAllThreads();
}
/*********************************************************************************
* 
*********************************************************************************/
std::vector<Entry> InvertedIndex::GetWordCount(const std::string &word)
{
    return freqDictionary[word];
}
