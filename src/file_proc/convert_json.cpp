#include "convert_json.hpp"
#include <fstream>

/*
* Метод получения содержимого файлов
* @return Возвращает список с содержимым файлов перечисленных
* в config.json
*/
ConverterJSON::ConverterJSON()
{
    std::ifstream ConfFile("./cfg/conf.json");
    if(!ConfFile.is_open()) { 
        ErrExcetp("File conf.json not found");
    }
    mainConf = nlohmann::json::parse(ConfFile);
    
    if(mainConf["config"].empty())
        ErrExcetp("Format error in conf.json file, config array not found");

    if(mainConf["files"].empty())
        ErrExcetp("Format error in conf.json file, files array not found");

    if(mainConf["config"]["name"].empty())
        ErrExcetp("Format error in conf.json file, name of project not found");

    if(mainConf["config"]["version"].empty())
        ErrExcetp("Format error in conf.json file, version of project not found");
    
    if(mainConf["config"]["max_responses"].empty())
        ErrExcetp("Format error in conf.json file, max count of responses not found");
    
    
    
    std :: cout << mainConf["config"]["name"] << std::endl; 
}
std::vector<std::string> ConverterJSON::GetTextDocuments()
{
    std::vector<std::string> docs;
    std::string text, buff;
    std::ifstream file;

    for(auto &it:mainConf["files"]){
        file.open(it);
        if(!file.is_open()) docs.emplace_back("");
        while(getline(file, buff)) text += " " + buff;
    
        docs.push_back(text);
        file.close();
        text.clear();
    }
    return docs;
}

int ConverterJSON::GetResponsesLimit()
{
    return mainConf["config"]["max_responses"];
}

std::vector<std::string> ConverterJSON::GetRequests()
{
    std::ifstream ConfFile("./cfg/requests.json");

    if(!ConfFile.is_open()){
        ErrExcetp("File requests.json not found");
        return std::vector<std::string>();
    }

    nlohmann::json reqDit = nlohmann::json::parse(ConfFile);
    if(reqDit["requests"].empty()) {
        ErrExcetp("Format error in requests.json file, requests not found");
        return std::vector<std::string>();
    }
    
    return reqDit["requests"];
}

void ConverterJSON::putAnswers( std::vector<JSON_PAIR_VEC> &answers)
{
    std::ofstream ansFile("./cfg/answers.json", std::ofstream::trunc);
    nlohmann::json outDict;
    int i = 0;
    std::string req_name;
    auto add_str = [](int num, std::string &str){
        if(num/100 == 0) str = "00";
        else if(num/10 == 0) str = "0";
    };

    if(!ansFile.is_open()) return;

    for(auto &resIt:answers){
        i++;
        add_str(i, req_name);
        req_name = "request" + req_name + std::to_string(i);
        outDict[req_name]["result"] = (resIt.empty()) ? "false" : "true";
        for(auto &docIt : resIt){
            outDict[req_name]["relevance"].push_back({{"docid", docIt.first}, {"rank", (std::to_string(docIt.second)).erase(5)}});
        }
        req_name.clear();
    }
    ansFile << outDict.dump(4);
}

