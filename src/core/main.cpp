#include <iostream>
#include "convert_json.hpp"
#include "DocParser.hpp"
#include "search_server.hpp"
#include "common.hpp"

std::vector<JSON_PAIR_VEC> ConvertRelativeIndexToPair(std::vector<std::vector<RelativeIndex>> &RelIndexVec, int maxResCount){
    std::vector<JSON_PAIR_VEC> convRes;
    int size = 0;

    if(maxResCount <= 0 ) {
       DbgLog(" Not enough size for make JSON_PAIR_VECTOR");
        return convRes;
        }

    for(auto &request : RelIndexVec){
        JSON_PAIR_VEC req_docs;
        size = 0;
        for(auto &docInfo: request){
            if(size > maxResCount) break;
            size++;
            JSON_PAIR doc{docInfo.docId, docInfo.rank};
            req_docs.push_back(doc);
        }
        convRes.push_back(req_docs);
    }
    return convRes;
}

int main(int argc, char* argv[]){

    try {
    ConverterJSON mainConf;
    InvertedIndex dataBase;

    dataBase.UpdateDocumentBase(mainConf.GetTextDocuments());
    
    SearchServer srv(dataBase);
    std::vector<std::string> search_req = mainConf.GetRequests();
    std::vector<std::vector<RelativeIndex>> searchResult = srv.search(search_req);

    if(searchResult.empty()){
        std::cout << "All requests is empty" << std::endl;
        return 0;
    } 
    std::vector<JSON_PAIR_VEC> resultJSON = ConvertRelativeIndexToPair(searchResult, mainConf.GetResponsesLimit());

    mainConf.putAnswers(resultJSON);    
    }
    catch(std::exception &x){ return -1;}

    std::cout << "Engin work is successfull" << std::endl;
    return 0;
}
