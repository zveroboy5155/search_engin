#include "search_server.hpp"
#include <sstream>
#include <stack>
#include <algorithm>

template <typename T1, typename T2>
struct lessSecond {
    typedef std::pair<T1, T2> type;
    bool operator ()(type const& a, type const& b) const {
        return a.second > b.second;
    }
};

std::vector<RelativeIndex> SearchServer::TransformToRelInxStruct(std::map<int, int> &docsMap)
{
    std::vector <RelativeIndex> relVec;
    std::vector<std::pair<int, int> > mapcopy(docsMap.begin(), docsMap.end());
    
    std::sort(mapcopy.begin(), mapcopy.end(), lessSecond<int, int>());

    for(auto &doc: mapcopy){
        RelativeIndex newInd;
        newInd.docId = doc.first;
        newInd.rank = doc.second;
        relVec.push_back(newInd);
    }

    return relVec;
}

SearchServer::SearchServer(InvertedIndex &idx) : _index(idx){};

std::vector<std::vector<RelativeIndex>> SearchServer::search(const std::vector<std::string> &queriesInput)
{
    std::vector<std::vector<RelativeIndex>> reqResult;
    std::vector<Entry> wordRes;

    for(auto &request:queriesInput){
        std::vector<RelativeIndex> requesRes;
        std::map<std::string, int> requestDict = DocProcessing(request);
        int maxAbsRank = 0;
        std::map<int, int> docsCollect;

        for(auto &word: requestDict){
            wordRes = _index.GetWordCount(word.first);
            
            //Indexing all docs by word 
            for(auto &doc: wordRes){
                if(docsCollect.find(doc.docId) == docsCollect.end()) 
                    docsCollect.insert(std::pair<int, int>(doc.docId, 0));
                docsCollect[doc.docId] += doc.count;
            }
        }
        //search max abs rank
        for(auto &cur_doc:docsCollect) 
            if(maxAbsRank < cur_doc.second) maxAbsRank = cur_doc.second;

        requesRes = TransformToRelInxStruct(docsCollect);

        //normalize rank
        for(auto &cur_doc:requesRes) cur_doc.rank /= maxAbsRank;
        
        reqResult.push_back(requesRes);
    }
    return reqResult;
}
