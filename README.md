<h1 align="center">SearchEngine</h1>
<h2 align="center">

## Description
This is a program for searching documents in a pre-indexed database. The behavior of the program is controlled using files "**conf.json**" and "**requests.json**". The results are displayed in a "answers.json" file.

## How to work
Before starting the program, you need to fill in the files "**{EXECCUTABLE_DIR}/cfgconf.json**" and "**{EXECCUTABLE_DIR}/cfgrequests.json**". After work in "**{EXECCUTABLE_DIR}/cfg/answers.json**" will contain the search results. If answers.the json was not empty, then it will be cleared before writing.

### Description config file conf.json 
File with the main settings for the program.

#### Structure

### Description config file requests.json
File with search queries that the program will search for.

#### Structure

### Description config file answers.json
Create in "**{EXECCUTABLE_DIR}/cfg/**" after work program. Contains search results.

#### Structure
