cmake_minimum_required (VERSION 3.5)
project (search_engine)

set(CMAKE_CXX_STANDARD 14)
##Work with Google test content
include(FetchContent)
FetchContent_Declare(
    googletest
    URL
    https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
set(gtest_disable_pthreads on)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)
enable_testing()
##Work with nlohmann json content
FetchContent_Declare(
    json
    URL 
    https://github.com/nlohmann/json/releases/download/v3.11.2/json.tar.xz
)
set(JSON_BuildTests OFF CACHE INTERNAL "")
FetchContent_MakeAvailable(json)
##Main target prepare
include_directories(./include)
file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "src/**.cpp")

add_executable(search_engine ${SOURCES})
##Copy conf/resources files
add_custom_command(TARGET search_engine PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/cfg $<TARGET_FILE_DIR:${PROJECT_NAME}>/cfg)

add_custom_command(TARGET search_engine PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/resources $<TARGET_FILE_DIR:${PROJECT_NAME}>/resources)

#Linking
target_link_libraries(search_engine PRIVATE nlohmann_json::nlohmann_json)
##Testing programm prepare
file(GLOB_RECURSE TEST_SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "src/file_proc/**.cpp" "src/search/**.cpp")
add_executable(mytest "./tests/mytests.cpp" ${TEST_SOURCES})
target_link_libraries(mytest PRIVATE gtest_main)
include(GoogleTest)
gtest_discover_tests(mytest)
target_link_libraries(mytest PRIVATE nlohmann_json::nlohmann_json)