#pragma once
#include <vector>
#include <string>
#include <map>
#include <mutex>
#include <thread>

struct Entry {
    size_t docId, count;

    Entry(int id, int count) : docId(id), count(count){};

    bool operator ==(const Entry& other) const {
        return (docId == other.docId &&
                count == other.count);
    }
};

class DocParser{
    
    void UpdateGlobDict(std::map<std::string, int> locDict, size_t docId);
protected:
    std::map<std::string, std::vector<Entry>> freqDictionary; // частотный словарь
    std::map<std::string, int> DocProcessing(std::string text, size_t docId = -1);
};

class InvertedIndex : public DocParser
{
private:
    std::vector<std::string> docs; // список содержимого документов
    
public:
    InvertedIndex() = default;
    /**
    * Обновить или заполнить базу документов, по которой будем совершать
    поиск*
    @param texts_input содержимое документов (Провести полную индексацию)
    */
    void UpdateDocumentBase(std::vector<std::string> input_docs);
    /*
    * Метод определяет количество вхождений слова word в загруженной базе
    документов
    * @param word слово, частоту вхождений которого необходимо определить
    * @return возвращает подготовленный список с частотой слов
    */
    std::vector<Entry> GetWordCount(const std::string& word);
};
