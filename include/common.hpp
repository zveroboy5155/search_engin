#pragma once
#include <iostream>
#include <exception>


#define DBG std::cout << __FILE__ << ":" << __LINE__<< " "<< __func__<< std::endl
#define DbgLog(data) std::cout << __FILE__ << ":" << __LINE__<< " "<< __func__<<" "<< data << std::endl
#define JSON_PAIR std::pair<int, float>
#define JSON_PAIR_VEC std::vector<JSON_PAIR>
#define ErrExcetp(msg) {std :: cout << msg <<std::endl;\
                       throw std::exception();}

