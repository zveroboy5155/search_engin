#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "nlohmann/json.hpp"
#include "common.hpp"

/**
* Класс для работы с JSON-файлами
*/
class ConverterJSON {
private:
    nlohmann::json mainConf;
public:
    ConverterJSON();
/**
* Метод получения содержимого файлов
* @return Возвращает список с содержимым файлов перечисленных
* в config.json
*/
    std::vector<std::string> GetTextDocuments();
/**
* Метод считывает поле max_responses для определения предельного
* количества ответов на один запрос
* @return
*/
    int GetResponsesLimit();
/**
* Метод получения запросов из файла requests.json
* @return возвращает список запросов из файла requests.json
*/
    std::vector<std::string> GetRequests();
/**
* Положить в файл answers.json результаты поисковых запросов
*/
    void putAnswers( std::vector<JSON_PAIR_VEC> &answers);
};